const CookieClicker = (props) => { 
  const [score, setScore] = React.useState(props.scoreInitial)
  const addOne = () => setScore(score + 1)
  return <div> 
      <SimpleClick score={score} onClick={addOne}/>
      <ClickUpgrade score={score}/>
   </div>
}

const SimpleClick = (props) => {
  return <button onClick={props.onClick}>{props.score}</button>
}

const ClickUpgrade = (props) => {
  console.log(props)
  if (props.score >= 10) {
    return <div>ok</div>
  }
  return <div>not ok</div>
}



ReactDOM.render(
  <div>
    <CookieClicker scoreInitial={0}/>
  </div>,
  document.getElementById('root') 
);