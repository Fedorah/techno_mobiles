import logo from './logo.svg';
import './App.css';
import React from 'react';

function BoutonMagique (props) {
  return <button>{props.text}</button>;
}

function Cookie (props) {
  const [score, setScore] = React.useState(props.scoreInitial)
  const addOne = () => setScore(score + 1)
  return <div> 
      <SimpleClick score={score} onClick={addOne}/>
      <ClickUpgrade score={score}/>
   </div>
}

function SimpleClick (props) {
  return <button onClick={props.onClick}>{props.score}</button>
}

function ClickUpgrade (props) {
  console.log(props)
  if (props.score >= 10) {
    return <div>ok</div>
  }
  return <div>not ok</div>
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Cookie scoreInitial={0}/>
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
